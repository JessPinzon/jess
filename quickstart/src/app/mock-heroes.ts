import { Hero } from './hero';

export const HEROES: Hero[] = [
  { id: 1, name: 'Sheldon Cooper', img: 'http://vignette1.wikia.nocookie.net/bigbangtheory/images/7/77/Jp2.jpg/revision/latest?cb=20121014173454' },
  { id: 2, name: 'Leonard Hofstader', img: 'https://s-media-cache-ak0.pinimg.com/736x/f3/09/7a/f3097ae1a098725e4bea51f74b53db8a.jpg' },
  { id: 3, name: 'Howard Wolowitz', img: 'https://s-media-cache-ak0.pinimg.com/originals/19/3a/e8/193ae8ddbb809c3387244014178c3cd4.jpg' },
  { id: 4, name: 'Rajesh Kootrophali', img: 'https://alexdobbieinindia.files.wordpress.com/2011/05/dr-rajesh-ramayan-koothrappali.jpg' },
  { id: 5, name: 'Penny', img: 'https://advancedgraphics.com/wp-content/uploads/2013/08/1331_Penny_28.jpg' },
  { id: 6, name: 'Amy Farrah', img: 'http://vignette3.wikia.nocookie.net/bigbangtheory/images/6/60/Amy.jpg/revision/latest?cb=20130319151544' },
  { id: 7, name: 'Emlily', img: 'http://img3.wikia.nocookie.net/__cb20150303175118/bigbangtheory/images/thumb/1/13/Col8.jpg/310px-Col8.jpg' },
  { id: 8, name: 'Stuart', img: 'http://the-big-bang-theory.com/images/uploads/1/rs_2174f98691bfb1c140d.jpg' },
  { id: 9, name: 'Kripke', img: 'http://vignette2.wikia.nocookie.net/the-big-bang-theory/images/8/8d/Barry_kripke.png/revision/latest?cb=20130705000223&path-prefix=fr' },
  { id: 10, name: 'Benadette', img: 'https://secure.polyvoreimg.com/cgi/img-thing/size/l/tid/76083692.jpg' }
    ];